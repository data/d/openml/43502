# OpenML dataset: Online-P2P-Lending

https://www.openml.org/d/43502

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

P2P Lending
I concatenated historical loans from both Prosper and Lending Club 2013 - 2018.  Currently only the summary of the loan (terms, origination date, loan amount, status, etc) are up but detailed lender data will come soon.  The columns are matched up as accurately as possible but there are estimated columns, see below for more info.
Content
To come.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43502) of an [OpenML dataset](https://www.openml.org/d/43502). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43502/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43502/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43502/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

